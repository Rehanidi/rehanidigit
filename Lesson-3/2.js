/**
 * Задача 2.
 *
 * Исправить функцию checkSpam(source, spam)
 * Функция возвращает true, если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров на тип string.
 * - Функция должна быть нечувствительна к регистру
 */

// РЕШЕНИЕ
function checkSpam(source, spam) {

    if (typeof source !== 'string') {
        return console.log('String is not a string');
    } else if (typeof  spam !== 'string') {
        return console.log('Substring in not a string');
    }

    let search_str = new RegExp(spam, "gi");
    const result = source.search(search_str);
    return (result > 0) ? true : false;
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false
