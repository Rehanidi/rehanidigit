/**
 * Задача 1.
 *
 * Исправить функцию upperCaseFirst(str).
 * Функция преобразовывает первый символ переданной строки в заглавный и возвращает новую строку.
 *
 * Условия:
 * - Функция принимает один параметр;
 * - Необходимо проверить что параметр str является строкой
 */

// РЕШЕНИЕ
function upperCaseFirst(str) {
    const result = str;

    if (typeof str !== "string") {
        return console.log("The entered value is not a string!");
    }

    const newStr = str.charAt(0).toUpperCase() + str.slice(1);
    return newStr;
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''
