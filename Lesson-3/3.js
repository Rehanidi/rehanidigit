/**
 * Задача 3.
 *
 * Исправить функцию truncate(string, maxLength).
 * Функция проверяет длину строки string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * Результатом функции должна быть (при необходимости) усечённая строка.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров;
 * - Первый параметр должен обладать типом string;
 * - Второй параметр должен обладать типом number.
 */

// РЕШЕНИЕ
function truncate(string, maxLength) {
    let tmpString;
    let tmpEnd = '...';

    if (typeof string !== 'string'){
        return console.log('Not a string!');
    } else if (typeof maxLength !== 'number') {
        return console.log('Not a number!');
    } else if (maxLength < tmpEnd.length) {
        return console.log('Your max length too short!');
    } else if (string.length === 0){
        return console.log("String is empty!")
    }

    if (string.length > maxLength) {
        tmpString = string.slice(0, maxLength - 3) + tmpEnd;
        return tmpString;
    }

    return string;
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'

