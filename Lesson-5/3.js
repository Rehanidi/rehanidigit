/**
 * Задача 6.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

// РЕШЕНИЕ
function isEven(number){
    if (typeof number !== 'number'){
        throw new Error('Argument is not a number type!')
    }
    return number % 2 === 0;
}

isEven(3); // false
isEven(4); // true

try {
    console.log(isEven(3));
    console.log(isEven(4));
    console.log(isEven('a'));
} catch (e) {
    console.log(e.message);
}
