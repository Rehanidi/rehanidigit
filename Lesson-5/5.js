/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ
function createFibonacciGenerator() {
    let fbNumber = 0;
    let tmp1, tmp2;
    return {
        print: () => {
            if (fbNumber === 0){
                tmp1 = ++fbNumber;
                return tmp1;
            }
            if (fbNumber === 1 || fbNumber === 2) {
                tmp2 = fbNumber++;
                return tmp2;
            }
            fbNumber = tmp1 + tmp2;
            tmp1 = tmp2;
            tmp2 = fbNumber;
            return fbNumber;
        },
        reset: () => {
            fbNumber = 0;
        },
    };
}


const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

exports.createFibonacciGenerator = createFibonacciGenerator;
