/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ
function f() {
    let count = 0;
    for (let i = 0; i < arguments.length; i++){
        if (typeof arguments[i] !== 'number'){
            throw new Error('One of arguments are not a number');
        }
        count += arguments[i];
    }
    return count;
}

try {
    console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
} catch (e){
    console.log(e.message);
}
