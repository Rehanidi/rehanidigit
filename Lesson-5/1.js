/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ
function f2 (number){
    if (typeof number !== 'number'){
        throw new Error('Value is not a number!')
    }
    return Math.pow(number, 3);
}
try{
    console.log(f2(2)); // 8
} catch (e) {
    console.log(e.message);
}
