/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

// Решение

function filter (arr, callback) {
    let length = arr.length;
    let results = [];

    try{
        if(!Array.isArray(arr)){
            throw new Error;
        }
    } catch (e){
        return 'First argument is not a array!';
    }

    try{
        if(typeof callback !== 'function'){
            throw new Error;
        }
    } catch (e){
        return 'Second argument is not a function!';
    }

    for (let i = 0; i < length; i++) {
        if (callback(arr[i], i, arr)) {
            results.push(arr[i]);
        }
    }
    return results;
};

const filteredArray = filter(array, (element, index, arrayRef) => {
    return element === 'Добрый вечер!';
});

console.log(filteredArray); // ['Добрый вечер!']
