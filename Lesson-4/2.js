/**
 * Задача 5.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение

function reduce(arr, callback, startValue){
    let length = arr.length;
    let results = startValue;

    try{
        if (!Array.isArray(arr)){
            throw new Error;
        }
    } catch (e){
        return 'The first argument is not an array!';
    }

    try {
        if (typeof callback !== 'function'){
            throw new Error;
        }
    } catch (e){
        return 'The second argument is not a function!';
    }

    for ( let i = 0; i < length; i++){
        if (typeof results === 'undefined'){
            results = arr[i];
            i++;
        }
        results = callback(results, arr[i], i, arr);
    }
    return results;
}

const result = reduce(
    array,
    (accumulator, element, index, arrayRef) => {
        //console.log(`${index}:`, accumulator, element, arrayRef);

        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

console.log(result); // 21
