/**
 * Задача 3.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

function createArray(value, arrayLength){
    const array = [];

    if (typeof value === 'Boolean ' ||
            typeof value == null ||
                typeof value === 'undefined' ||
                    typeof value === 'Symbol'){
        return 'First argument has a type error!'
    }

    if (typeof arrayLength !== 'number'){
        return 'Array length must be a number!'
    }

    for (let i = 0; i < arrayLength; i++){
        array[i] = value;
    }
    return array;
}

const result = createArray('x', 5);;

console.log(result); // [ x, x, x, x, x ]

exports.createArray = createArray;
