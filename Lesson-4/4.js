/**
 * Задача 2.
 *
 * Напишите функцию `collect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — число.
 * Массив, который передаётся в аргументе может быть одноуровневым или многоуровневым.
 * Число, которое возвращает функция должно быть суммой всех элементов
 * на всех уровнях всех вложенных массивов.
 *
 * Если при проходе всех уровней не было найдено ни одного числа,
 * то функция должна возвращать число 0.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива reduce.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - Если на каком-то уровне было найдено не число и не массив.
 */

// Решение

function getArrayDepth(obj) {
    if (Array.isArray(obj)) {
        return 1 + Math.max(...obj.map(t => getArrayDepth(t)))
    } else {
        return 0;
    }
}

function collect(array){
    if (!Array.isArray(array)){
        throw new Error('Object is not a array!');
    }

    const tmpArray = array.flat(getArrayDepth(array));
    let count = 0;
    for (let i = 0; i < tmpArray.length; i++) {
        if (typeof tmpArray[i] === 'number') {
            count++;
        }

        if (Number.isNaN(tmpArray[i])){
            throw new Error("Array have a NaN!")
        }
    }

    if (count === 0) {
        return 0;
    }

    const newArray = tmpArray.reduce(function (accumulator, currentValue) {
        if (typeof accumulator !== 'number' || typeof currentValue !== 'number') {
            throw new Error('Some value is not correct!');
        }
        return accumulator + currentValue;
    });

    return newArray;
}

try {
    const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
    console.log(collect(array1)); // 12

    const array2 = [[[[[1, 2]]]]];
    console.log(collect(array2)); // 3

    const array3 = [[[[[1, 2]]], 2], 1];
    console.log(collect(array3)); // 6

    const array4 = [[[[[]]]]];
    console.log(collect(array4)); // 0

    const array5 = [[[[[], 3]]]];
    console.log(collect(array5)); // 3

    const array6 = [NaN];
    console.log(collect(array6)); // error
} catch (e){
    console.error(e.message);
}

try {
    const array7 = 12;
    console.log(collect(array7)); // error
} catch (e){
    console.error(e.message);
}

exports.collect = collect;
