/**
 * Напишите функцию для форматирования даты.
 *
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 *
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
*/

const formatDate = (date, format, delimiter) => {
    let newDate = [];

    if (typeof delimiter === 'undefined'){
        delimiter = '.';
    }

    if (!(delimiter === '-' || delimiter === '.' || delimiter === '/')){
        return 'Delimiter is incorrect!';
    }

    let dd = date.getDate();
    if (dd < 10) {
        dd = `0${dd}`;
    }

    let mm = date.getMonth();
    if (mm < 10) {
        mm = `0${mm}`;
    }

    let yy = date.getFullYear();

    let array = format.split(' ');

    for (let i = 0; i < array.length; i++) {
        if (array[i] === 'DD' || array[i] === 'MM' || array[i] === 'YYYY') {
            if (array[i] === 'DD') {
                newDate.push(dd);
            }
            if (array[i] === 'MM') {
                newDate.push(mm);
            }
            if (array[i] === 'YYYY') {
                newDate.push(yy);
            }
        } else {
            throw new Error('Incorrect date format');
        }
    }
    return newDate.join(delimiter);
};
try {
    console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
    console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
    console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
} catch (error) {
    console.log(error.message);
}
