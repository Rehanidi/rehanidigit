/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ
let articleDiv = document.querySelector("form");
let div = document.createElement('div');
div.setAttribute('class', 'form-group');
articleDiv.appendChild(div);

let label = document.createElement('label');
label.setAttribute('For', 'email');
label.textContent = 'Электропочта';
div.appendChild(label);


let input = document.createElement('input');
input.setAttribute('type', 'email');
input.setAttribute('class', 'form-control');
input.setAttribute('id', 'email');
input.setAttribute('placeholder', 'Введите свою электропочту');
div.appendChild(input);

let div1 = document.createElement('div');
div1.setAttribute('class', 'form-group');
articleDiv.appendChild(div1);

let label1 = document.createElement('label');
label1.setAttribute('For', 'password');
label1.textContent = 'Пароль';
div1.appendChild(label1);

let input1 = document.createElement('input');
input1.setAttribute('type', 'password');
input1.setAttribute('class', 'form-control');
input1.setAttribute('id', 'password');
input1.setAttribute('placeholder', 'Введите пароль');
div1.appendChild(input1);

let div2 = document.createElement('div');
div2.setAttribute('class', 'form-group form-check');
articleDiv.appendChild(div2);

let input2 = document.createElement('input');
input2.setAttribute('type', 'checkbox');
input2.setAttribute('class', 'form-check-input');
input2.setAttribute('id', 'exampleCheck1');
div2.appendChild(input2);

let label2 = document.createElement('label');
label2.setAttribute("class", "form-check-label");
label2.setAttribute("For", "exampleCheck1");
label2.textContent = 'Запомнить меня';
div2.appendChild(label2);

let btn = document.createElement('button');
btn.setAttribute('type', 'submit');
btn.setAttribute('class', 'btn btn-primary');
btn.textContent = 'Вход';
articleDiv.appendChild(btn);
