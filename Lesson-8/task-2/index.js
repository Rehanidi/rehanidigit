/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ
let articleDiv = document.querySelector("#form");
articleDiv.setAttribute('onsubmit', 'return false');

let div = document.createElement('div');
div.setAttribute('class', 'form-group');
articleDiv.appendChild(div);

let label = document.createElement('label');
label.setAttribute('For', 'email');
label.textContent = 'Электропочта';
div.appendChild(label);


let input = document.createElement('input');
input.setAttribute('type', 'email');
input.setAttribute('class', 'form-control');
input.setAttribute('id', 'email');
input.setAttribute('placeholder', 'Введите свою электропочту');
div.appendChild(input);

let div1 = document.createElement('div');
div1.setAttribute('class', 'form-group');
articleDiv.appendChild(div1);

let label1 = document.createElement('label');
label1.setAttribute('For', 'password');
label1.textContent = 'Пароль';
div1.appendChild(label1);

let input1 = document.createElement('input');
input1.setAttribute('type', 'password');
input1.setAttribute('class', 'form-control');
input1.setAttribute('id', 'password');
input1.setAttribute('placeholder', 'Введите пароль');
div1.appendChild(input1);

let div2 = document.createElement('div');
div2.setAttribute('class', 'form-group form-check');
articleDiv.appendChild(div2);

let input2 = document.createElement('input');
input2.setAttribute('type', 'checkbox');
input2.setAttribute('class', 'form-check-input');
input2.setAttribute('id', 'exampleCheck1');
div2.appendChild(input2);

let label2 = document.createElement('label');
label2.setAttribute("class", "form-check-label");
label2.setAttribute("For", "exampleCheck1");
label2.textContent = 'Запомнить меня';
div2.appendChild(label2);

let btn = document.createElement('button');
btn.setAttribute('type', 'submit');
btn.setAttribute('class', 'btn btn-primary');
btn.textContent = 'Вход';
articleDiv.appendChild(btn);

btn.onclick = function() {
    const obj = {};
    let email = document.getElementById('email');
    if (email.value === '' || email.value === ' ' ){
        alert('Email error');
        return false;
    }

    let password = document.getElementById('password');
    if (password.value === '' || password.value === ' ' ){
        alert('Password error');
        return false;
    }

    obj.email = document.getElementById('email').value;
    obj.password = document.getElementById('password').value;
    obj.remember = document.getElementById('exampleCheck1').checked;
    console.log(obj);
}
